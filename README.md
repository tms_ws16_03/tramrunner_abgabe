# README TramRunner #

## Was ist das? ##

Die App TramRunner macht es dem User möglich zu überprüfen, ob er in der Zeit, in der er
auf die Bahn wartet, auch zur nächsten Station laufen könnte. Das bietet ihm einen
sinnvolleren Zeitvertreib, als auf die Bahn zu warten.

Genauere Informationen zur Funktionsweise stehen im Konzept.

----------

## Getting Started ##

### Download der Release-APK ###

* Im Repository befindet sich eine APK-Datei, die auf einem Smartphone installiert werden kann.
* [Release-APK im Repository](https://bitbucket.org/tms_ws16_03/tramrunner_abgabe/src/5636a2ba707760b88cae8934b68db490abf90c35?at=master)


### Importieren des Projekts in eine Entwicklungs-Umgebung (z.b. [Android-Studio](https://developer.android.com/studio/index.html))

* Mithilfe der Konsole klonen Sie das Repository lokal auf Ihren Rechner.
* Wechseln Sie in das lokale Verzeichnis, in das das Repository geklont werden soll und führen sie einen der folgenden beiden Befehle aus:
	* HTTP: `git clone https://ihrNutzername@bitbucket.org/tms_ws16_03/tramrunner_abgabe.git`
	* SSH: `git clone git@bitbucket.org:tms_ws16_03/tramrunner_abgabe.git`
* Öffnen Sie das Projekt, indem Sie in Ihrer Entwicklungsumgebung das vorhandenen Projekt importieren.
	* Beispiel Android-Studio:
	
	 ![Import Project in Android Studio](http://www.tricedesigns.com/wp-content/uploads/2013/05/02-welcome.jpg)

### Starten der Unit-Tests ###

* zum Starten der Unit-Tests in Android-Studio im Projekt in den richtigen Ordner navigieren (app/src/androidTest/java/app/amazing/tramrunner/)
* mit einem Rechtsklick auf die Datei StatisticTest.java im Untermenü "Run" auswählen
* die Unit-Tests werden gestartet

-----------

## Einschränkungen ##

* Um die App nutzen zu können, muss das GPS aktiviert sein und es muss eine Internetverbindung bestehen. Sonst ist es nicht möglich die aktuelle Position abzufragen und Bahndaten zu erhalten, sowie die Route zu berechnen.
* Die nächste Station wird nur angezeigt, wenn sie sich in einem Umkreis von 1 km befindet.