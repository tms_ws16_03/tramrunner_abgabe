package app.amazing.tramrunner;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import app.amazing.tramrunner.data.StatisticData;

/**
 * Created by Delia on 24.01.2017.
 */
@RunWith(AndroidJUnit4.class)
public class StatisticTest {

    @Before
    public void setUp() {
        Context context = InstrumentationRegistry.getContext();
        StatisticData statistics = StatisticData.getInstance();
        statistics.init(context);
    }

    /**
     * check if method for converting meter into kilometer works fine
     */
    @Test
    public void convert_m_to_km() {
        final int meter = 30657;
        final float expected_km = 30.657f;

        float converted_meter = StatisticData.getInstance().changeMeterToKilometer(meter);

        Assert.assertEquals(converted_meter, expected_km, 0);
    }

    /**
     * check if a new distance is added correctly after a successful run
     */
    @Test
    public void addNewDistance() {
        float old_km = StatisticData.getInstance().getKilometers_run();
        final int new_meter = 1593;
        float convert_m_to_km = StatisticData.getInstance().changeMeterToKilometer(new_meter);
        float expected_new_km = old_km + convert_m_to_km;

        StatisticData.getInstance().addSuccessfulRunData(new_meter);
        float actual_new_km = StatisticData.getInstance().getKilometers_run();

        Assert.assertEquals(actual_new_km, expected_new_km, 0);
    }

    /**
     * check if deleteData really changes the saved values to its default values (0)
     */
    @Test
    public void deleteData() {
        StatisticData.getInstance().deleteData();
        Assert.assertSame(StatisticData.getInstance().getRuns_accomplished(), 0);
    }
}
