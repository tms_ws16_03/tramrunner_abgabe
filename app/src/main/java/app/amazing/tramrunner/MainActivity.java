package app.amazing.tramrunner;


import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.content.Intent;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

import android.widget.TextView;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.data.StatisticData;
import app.amazing.tramrunner.googledirections.DirectionListener;
import app.amazing.tramrunner.googledirections.MapsDirectionAPI;
import app.amazing.tramrunner.location.LocationContainer;
import app.amazing.tramrunner.vbb.VbbApiHelper;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import app.amazing.tramrunner.vbb.vbbdata.Station;
import app.amazing.tramrunner.googledirections.Route;

public class MainActivity extends AppCompatActivity implements DirectionListener {

    View startRunButton;    //start Run Button
    ProgressBar progressBarTram, progressBarRunner;
    ObjectAnimator animationTram, animationRunner;
    boolean runPossible = false;
    ArrayAdapter<Station> adapterStops;
    ArrayAdapter<Departure> adapterTrams;
    Spinner stopSpinner;
    Spinner tramSpinner;
    Date lastRequestTime;
    TextView nextStationText;
    TextView tramEstimateText;
    TextView runnerEstimateText;

    long lastCalculatedTramTime;
    int lastCalculatedStationDistance;
    Station lastCalculatedStation;
    Departure lastCalculatedDeparture;
    String lastCalculatedPolyline;

    final int MAX_PROGRESS = 65;

    public ArrayAdapter<Station> getAdapterStops() {
        return adapterStops;
    }

    public ArrayAdapter<Departure> getAdapterTrams() {
        return adapterTrams;
    }

    public Date getLastRequestTime() {
        return lastRequestTime;
    }

    public void setLastRequestTime(Date lastRequestTime) {
        this.lastRequestTime = lastRequestTime;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ResData.setContext(this);

        StatisticData statistics = StatisticData.getInstance();
        statistics.init(this);
        //TODO: delete before Abgabe bei Herrn Obermann
        //StatisticData.getInstance().deleteData();

        VbbApiHelper helper = VbbApiHelper.getInstance();
        helper.setHeader();
        helper.setActivity(this);
        stopSpinner = (Spinner) findViewById(R.id.station_spinner);
        tramSpinner = (Spinner) findViewById(R.id.tram_spinner);
        adapterStops = new ArrayAdapter<Station>(this, android.R.layout.simple_dropdown_item_1line);
        adapterTrams = new ArrayAdapter<Departure>(this, android.R.layout.simple_dropdown_item_1line);
        stopSpinner.setAdapter(adapterStops);
        tramSpinner.setAdapter(adapterTrams);

        stopSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setLastRequestTime(new Date());
                if (position != 0) {
                    Station temp = (Station) parent.getItemAtPosition(position);
                    VbbApiHelper.getInstance().getDepartures(temp.getId(), getLastRequestTime(), adapterTrams);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tramSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0) {
                    Departure temp = (Departure) parent.getItemAtPosition(position);
                    Station currentStation = (Station) stopSpinner.getSelectedItem();
                    VbbApiHelper.getInstance().getNextStation(currentStation.getLatitude(), currentStation.getLongitude(), temp, getLastRequestTime());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // initialize LocationContainer
        LocationContainer.getInstance(this);

        // check for GPS Permission (FINE_LOCATION)
        checkForGpsPermission();

        // bar labels

        nextStationText = (TextView) findViewById(R.id.textViewNextStationName);
        tramEstimateText = (TextView) findViewById(R.id.textViewTram);
        runnerEstimateText = (TextView) findViewById(R.id.textViewRunner);

        //CIrcular Progressbar from here
        progressBarTram = (ProgressBar) findViewById(R.id.progressBarTram);
        progressBarRunner = (ProgressBar) findViewById(R.id.progressBarRunner);

        animationTram = ObjectAnimator.ofInt(progressBarTram, getString(R.string.anim_property), 0, MAX_PROGRESS);
        animationRunner = ObjectAnimator.ofInt(progressBarRunner, getString(R.string.anim_property), 0, 100);

        animationTram.setDuration(3000); //in milliseconds
        animationTram.setInterpolator(new DecelerateInterpolator());

        animationRunner.setDuration(5000); //in milliseconds
        animationRunner.setInterpolator(new DecelerateInterpolator());

        animationTram.start();
        animationRunner.start();

        startRunButton = findViewById(R.id.startRunButton);

        //Button Setup from here
        registerStartRunButtonClickCallback();
        setStartRunButtonImage(false);


        //setup
        //applyCircularBarValues();


    }

    //lädt Buttons auf ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.info_button, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            //navigiert zum Statistikscreen beim Tippen auf das "i"
            case R.id.menu_item_statistics:
                Intent myIntent = new Intent(getApplicationContext(), StatisticsActivity.class);
                startActivityForResult(myIntent, 0);
                return true;
            //refresht den MainScreen und die location
            case R.id.menu_item_refresh:
                stopSpinner.setSelection(0, true);
                tramSpinner.setSelection(0, true);
                nextStationText.setText(getString(R.string.next_stationname));
                tramEstimateText.setText(getString(R.string.TramEstimateTime));
                runnerEstimateText.setText(getString(R.string.RunnerEstimateTime));
                adapterTrams.clear();
                LocationContainer.getLocationOnes(this);
                //Toast.makeText(MainActivity.this, "Refresh screen and location", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Starten einer Anfrage an die Google Maps Directions API
     *
     * @param origin      Startort
     * @param destination Ziel
     */
    public void sendDirectionRequest(Location origin, Station destination) {
        MapsDirectionAPI directionRequest = new MapsDirectionAPI(this, origin, destination);
        directionRequest.execute();
    }

    /**
     * wird aufgerufen, wenn eine Route gefunden wurde
     *
     * @param routes Liste mit der Route und evtl. alternativen Routen
     */
    @Override
    public void onDirectionRequestSuccess(List<Route> routes) {
        for (Route route : routes) {
            runnerEstimateText.setText(String.format(getString(R.string.runner_estimate_time), route.duration.getText()));
            long routeTime = TimeUnit.SECONDS.toMinutes(route.duration.getValue());
            lastCalculatedStationDistance = route.distance.getValue();
            lastCalculatedPolyline = route.encodedPolyline;
            //Log.d("[ROUTE TIME]: ", String.valueOf(routeTime));
            //Log.d("[TRAM TIME]: ", String.valueOf(lastCalculatedTramTime));

            if (routeTime > lastCalculatedTramTime) {
                animationRunner.setIntValues(0, MAX_PROGRESS);
                int val = (int) ((double) MAX_PROGRESS * ((double) lastCalculatedTramTime / (double) routeTime));
                System.out.println(val);
                animationTram.setIntValues(0, val);
                setStartRunButtonImage(false);
                //Log.d("[GREATER]", "than");
            } else {
                animationTram.setIntValues(0, MAX_PROGRESS);
                int val = (int) ((double) MAX_PROGRESS * ((double) routeTime / (double) lastCalculatedTramTime));
                System.out.println(val);
                animationRunner.setIntValues(0, val);
                setStartRunButtonImage(true);
                //Log.d("[LESS]", "than");
            }

            animationTram.start();
            animationRunner.start();
        }
    }

    /**
     * adds the onClick Listener to the start run Button in the circular_tram View
     */
    public void registerStartRunButtonClickCallback() {

        //regular Clicks are handeled here
        startRunButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                if (runPossible) {

                    startRun();

                }
            }
        });
    }

    /**
     * sets the image on the runPossible Button according to the possibilty
     *
     * @param runPossible
     */
    private void setStartRunButtonImage(boolean runPossible) {

        if (runPossible) {
            this.runPossible = true;
            startRunButton.setBackgroundResource(R.mipmap.start_run_possible);

        } else {
            this.runPossible = false;
            startRunButton.setBackgroundResource(R.mipmap.start_run_not_possible);
        }
    }

    /**
     * Method is called when a run can be initiated
     */
    private void startRun() {

        Intent intent = new Intent(MainActivity.this, RunningActivity.class);
        intent.putExtra(getString(R.string.extra_stationLat), Double.valueOf(lastCalculatedStation.getLatitude()));
        intent.putExtra(getString(R.string.extra_stationLon), Double.valueOf(lastCalculatedStation.getLongitude()));
        intent.putExtra(getString(R.string.extra_distanceTo), lastCalculatedStationDistance);
        intent.putExtra(getString(R.string.extra_tramDepartureTime), Long.valueOf(lastCalculatedDeparture.getWhen()) * 1000L);
        intent.putExtra(getString(R.string.extra_nextStation), lastCalculatedStation.toString());
        intent.putExtra(getString(R.string.extra_polyline), lastCalculatedPolyline);
        startActivity(intent);
    }

    public void checkForGpsPermission() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        } else {
            LocationContainer.getLocationOnes(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    // TODO hardcoded strings
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(getString(R.string.nogps_warning_content)).setTitle(getString(R.string.nogps_warning_title));
                    builder.setNeutralButton(getString(R.string.close_app), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            finish();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        }
    }

    public void setStartButtonValues(Station nextStation, Departure nextDeparture) {

        lastCalculatedStation = nextStation;
        lastCalculatedDeparture = nextDeparture;
        nextStationText.setText(nextStation.getName());
        int cond = Integer.valueOf(nextDeparture.getTrip());

        if (cond >= 0) {
            long tramTime = (Long.valueOf(nextDeparture.getWhen()) * 1000L) - new Date().getTime();
            Date date = new Date(tramTime);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            lastCalculatedTramTime = cal.get(Calendar.MINUTE);
            tramEstimateText.setText(String.format(getString(R.string.tram_estimate_time), lastCalculatedTramTime));
        }

        sendDirectionRequest(LocationContainer.getLastKnownLocation(), nextStation);
    }
}

