package app.amazing.tramrunner;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.GoogleApiAvailability;

import app.amazing.tramrunner.data.StatisticData;

public class StatisticsActivity extends AppCompatActivity {

    TextView runsAccomplishedText;
    TextView kilometersRunText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);


        runsAccomplishedText = (TextView) findViewById(R.id.labelRunsDone);
        kilometersRunText = (TextView) findViewById(R.id.labelKilometersRun);

        setData();

        //Zurück-Pfeil auf dem Actionbar erzeugen
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    //load statisitics data
    public void setData() {
        runsAccomplishedText.setText(String.valueOf(StatisticData.getInstance().getRuns_accomplished()));
        kilometersRunText.setText(String.format(getString(R.string.statistics_kilometers_value_label), String.valueOf(StatisticData.getInstance().getKilometers_run())));
    }

    //open dialog with apache or google open source license
    public void showLicenseDialog(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.pop_up_license);
        dialog.setCanceledOnTouchOutside(false);
        TextView license_title = (TextView) dialog.findViewById(R.id.label_license_title);
        TextView license_tv = (TextView) dialog.findViewById(R.id.label_license);
        Button done = (Button) dialog.findViewById(R.id.button_done_with_reading);

        if (view == view.findViewById(R.id.button_apacheLicense)) {
            license_title.setText(getString(R.string.apache_license));

            //load apache license
            license_tv.setText(getString(R.string.apache_license_value));
        } else {
            license_title.setText(getString(R.string.google_open_source_license));

            //load google open source license
            GoogleApiAvailability apiLicense = GoogleApiAvailability.getInstance();
            String googleLicense = apiLicense.getOpenSourceSoftwareLicenseInfo(this);
            license_tv.setText(googleLicense);
        }
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
