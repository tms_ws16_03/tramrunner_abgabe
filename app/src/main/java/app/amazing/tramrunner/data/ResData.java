package app.amazing.tramrunner.data;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by igor on 23.01.17.
 */

public class ResData {
    private static Context c;
    public static Resources r;

    public static void setContext(Context c) {
        ResData.c = c;
        ResData.r = c.getResources();
    }
}
