package app.amazing.tramrunner.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import app.amazing.tramrunner.R;

/**
 * Created by Delia on 22.01.2017.
 */

public class StatisticData {

    private Context context;

    private static StatisticData statisticInstance = new StatisticData();

    SharedPreferences preferences;

    //TODO: decide between getting rid of hardcoded Strings or keeping constants (or do some magic to get both)
    public static final String USER_PREFERENCES = "Prefs";
    private static final String RUNS_ACCOMPLISHED_KEY = "runs_accomplished";
    private static final String KILOMETERS_RUN_KEY = "kilometers_run";

    private int runs_accomplished_value;
    private float kilometers_run_value;

    /**
     * initialize context
     *
     * @param context
     */
    public void init(Context context) {
        this.context = context.getApplicationContext();
        preferences = context.getSharedPreferences(USER_PREFERENCES, Context.MODE_PRIVATE);
    }

    private StatisticData() {
    }

    /**
     * get Instance of StatisticsData
     *
     * @return Instance of StatisticsData
     */
    public static StatisticData getInstance() {
        return statisticInstance;
    }

    /**
     * get number of runs accomplished
     *
     * @return number of runs accomplished
     */
    public int getRuns_accomplished() {
        return preferences.getInt(RUNS_ACCOMPLISHED_KEY, 0);
    }

    /**
     * get distance of all successful runs together until now
     *
     * @return distance of all runs
     */
    public float getKilometers_run() {
        return preferences.getFloat(KILOMETERS_RUN_KEY, 0);
    }

    //TODO: delete if not needed
    /*public void setRuns_accomplished(int runs_accomplished) {
        runs_accomplished_value++;
    }

    public void setKilometers_run(int kilometers_run) {
        kilometers_run_value += kilometers_run;
    }*/

    /**
     * method called after a successful run
     *
     * @param new_meters meters to add to all time distance
     */
    public void addSuccessfulRunData(int new_meters) {
        //TODO: delete
        //System.out.println(context.getString(R.string.app_name)); //interessant, dass es hier funktioniert

        runs_accomplished_value = preferences.getInt(RUNS_ACCOMPLISHED_KEY, 0);
        runs_accomplished_value++;

        kilometers_run_value = preferences.getFloat(KILOMETERS_RUN_KEY, 0);
        kilometers_run_value += changeMeterToKilometer(new_meters);

        saveData();
    }

    /**
     * save new data in shared preferences
     */
    private void saveData() {

        SharedPreferences.Editor editor = preferences.edit();

        editor.putInt(RUNS_ACCOMPLISHED_KEY, runs_accomplished_value).apply();
        editor.putFloat(KILOMETERS_RUN_KEY, kilometers_run_value).apply();

        //editor.commit();
        //String dataSaved = context.getString(R.string.statistics_data_saved) + preferences.getFloat(KILOMETERS_RUN_KEY, 0) + " " + preferences.getInt(RUNS_ACCOMPLISHED_KEY, 0);
        //Log.d(context.getString(R.string.statistics_log), dataSaved);
    }

    /**
     * reset sharedPreference data
     */
    public void deleteData() {
        SharedPreferences.Editor editor = preferences.edit();

        editor.clear();
        editor.commit();
    }

    /**
     * method to change new distance from meter to kilometer
     *
     * @param new_meter meter to convert into kilometer
     * @return meter converted into kilometer
     */
    public float changeMeterToKilometer(int new_meter) {
        float new_kilometer = (float) new_meter / 1000;
        return new_kilometer;
    }
}
