package app.amazing.tramrunner.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import app.amazing.tramrunner.BuildConfig;
import app.amazing.tramrunner.R;
import app.amazing.tramrunner.location.LocationContainer;
import app.amazing.tramrunner.location.RunningLocationListener;
import app.amazing.tramrunner.runningProgress.RunningProgressbar;

/**
 * Created by gee on 24/01/2017.
 */

public class RunActiveProgressFragment extends Fragment {

    private static final String TAG = "RunActiveProgress";

    private ProgressBar pb;
    private View tramPin, runnerPin;
    private ImageView tramPinImage, runnerPinImage;
    private int[] loc = new int[2];
    private int[] pbLoc = new int[2];
    private int screenWidth, pbSize;
    Integer tramPinInitialPosition, runnerPinInitialPosition;
    RunningProgressbar runningProgressbar;
    RunningLocationListener listener;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.run_active_progress, container, false);

    }

    @Override
    public void onStart() {
        super.onStart();

        //get display dimension and stuff
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;

        pb = (ProgressBar) getView().findViewById(R.id.progressBar2);

        pb.setProgress(0);
        pb.setSecondaryProgress(0);
        pb.setMax(screenWidth);

        //ref to Progressbar Object
        runningProgressbar = new RunningProgressbar(this);

        //Pins
        tramPin = getView().findViewById(R.id.tram_pin_prefab);
        runnerPin = getView().findViewById(R.id.runner_pin_prefab);
        tramPinImage = (ImageView) tramPin.findViewById(R.id.imageView);
        runnerPinImage = (ImageView) runnerPin.findViewById(R.id.imageView);

        //set Images
        tramPinImage.setImageResource(R.mipmap.tram_pin);
        runnerPinImage.setImageResource(R.mipmap.runner_pin);


        //connect to new locations
        double stationLat = getActivity().getIntent().getExtras().getDouble(getActivity().getResources().getString(R.string.extra_stationLat));
        double stationLng = getActivity().getIntent().getExtras().getDouble(getActivity().getResources().getString(R.string.extra_stationLon));
        long tramDepartureTime = getActivity().getIntent().getExtras().getLong(getActivity().getResources().getString(R.string.extra_tramDepartureTime));
        listener = LocationContainer.updateLocationUntilFinished(getActivity(), stationLat, stationLng, tramDepartureTime, runningProgressbar);
    }

    /**
     * Progressbar is set to screensize so value is determined via 0 to 100 to 0 to screensize
     *
     * @param value put in (int from 0 to 100)
     * @return is a value according to the screensize
     */
    private int calculateProgress(int value) {
        float calculator = screenWidth / 100.0f;
        float absoluteProgress = value * calculator;
        return (int) absoluteProgress;
    }

    /**
     * sets the tramProgress to a value between 0 and 100%
     *
     * @param progress
     */
    public void setTramProgress(int progress) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "set Tram progress to: " + progress);
        int absoluteProgress = 0;

        if (progress < 0)
            absoluteProgress = calculateProgress(0);

        if (progress > 100)
            absoluteProgress = calculateProgress(100);

        if (progress >= 0 && progress <= 100) {
            absoluteProgress = calculateProgress(progress);
        }

        pb.setProgress(absoluteProgress);
        setTramPin(absoluteProgress);
        pb.requestLayout();
    }

    /**
     * sets the runnerProgress to a value between 0 and 100%
     *
     * @param progress
     */
    public void setRunnerProgress(int progress) {
        int absoluteProgress = 0;

        if (progress < 0)
            absoluteProgress = calculateProgress(0);

        if (progress > 100)
            absoluteProgress = calculateProgress(100);

        if (progress >= 0 && progress <= 100) {
            absoluteProgress = calculateProgress(progress);
        }

        pb.setSecondaryProgress(absoluteProgress);
        setRunnerPin(absoluteProgress);
        pb.requestLayout();
    }

    /**
     * sets the tram pin on the screen according to the progressbar position
     */
    private void setTramPin(int progress) {
        //get initial poisition on screen
        if (tramPinInitialPosition == null) {
            tramPin.getLocationOnScreen(loc);
            tramPinInitialPosition = new Integer(loc[0]);
        }

        tramPin.setTranslationX(progress);
        tramPin.setX(progress - getMultiplier(progress) * tramPinInitialPosition - getMultiplier(progress) * (tramPin.getWidth() / 2));
    }

    /**
     * sets the runnerpin on the screen according to the progressbar position
     */
    private void setRunnerPin(int progress) {
        //get initial poisition on screen
        if (runnerPinInitialPosition == null) {
            runnerPin.getLocationOnScreen(loc);
            runnerPinInitialPosition = new Integer(loc[0]);
        }

        runnerPin.setTranslationX(progress);
        runnerPin.setX(progress - getMultiplier(progress) * runnerPinInitialPosition - getMultiplier(progress) * (runnerPin.getWidth() / 2));
    }

    /**
     * returns a value between 0.1 and 2.0 according to progress from 0 to 100
     *
     * @param progress
     * @return
     */
    float getMultiplier(float progress) {
        float screen = screenWidth;

        if (progress > 0) {
            return progress * (2 / screen);
        } else return 0.1f;
    }

    /**
     * removes the LocationListener from the locationmanager to stop tracking
     */
    public void removeRunningListener() {
        try {
            LocationContainer.getLocationManager().removeUpdates(listener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }
}
