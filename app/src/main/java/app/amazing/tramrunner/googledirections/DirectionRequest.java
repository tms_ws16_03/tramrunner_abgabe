package app.amazing.tramrunner.googledirections;

import android.os.AsyncTask;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.googledirections.DirectionListener;
import app.amazing.tramrunner.googledirections.Route;
import app.amazing.tramrunner.googledirections.Distance;
import app.amazing.tramrunner.googledirections.Duration;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.google.maps.android.PolyUtil;

import static com.google.maps.android.PolyUtil.decode;

/**
 * Created by Delia on 20.01.2017.
 */

/**
 * sendet einen Request und empfängt, verarbeitet die Antwort
 */
public class DirectionRequest extends AsyncTask<String, Void, String> {

    private DirectionListener listener;

    public DirectionRequest(DirectionListener l) {
        this.listener = l;
    }

    /**
     * Senden der Anfrage an url (google maps directions api)
     *
     * @param params url
     * @return Antwort auf die Anfrage
     */
    protected String doInBackground(String... params) {
        String link = params[0];

        try {
            URL url = new URL(link);
            InputStream is = url.openConnection().getInputStream();
            StringBuffer buffer = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line + "\n");
            }

            return buffer.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Methode wird nach dem Erhalten einer Anfrage aufgerufen
     *
     * @param res Antwort auf die Anfrage
     */
    protected void onPostExecute(String res) {
        try {
            parseJSON(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verarbeiten der Antwort vom Request
     *
     * @param data Antwort auf die Anfrage
     * @throws JSONException
     */
    private void parseJSON(String data) throws JSONException {
        if (data == null) {
            return;
        }

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray(ResData.r.getString(R.string.param_routes));
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            JSONObject overview_polylineJSON = jsonRoute.getJSONObject(ResData.r.getString(R.string.param_overview_polyline));
            JSONArray jsonLegs = jsonRoute.getJSONArray(ResData.r.getString(R.string.param_legs));
            JSONObject jsonLeg = jsonLegs.getJSONObject(0);
            JSONObject jsonDistance = jsonLeg.getJSONObject(ResData.r.getString(R.string.param_distance));
            JSONObject jsonDuration = jsonLeg.getJSONObject(ResData.r.getString(R.string.param_duration));
            JSONObject jsonEndLocation = jsonLeg.getJSONObject(ResData.r.getString(R.string.param_end_location));
            JSONObject jsonStartLocation = jsonLeg.getJSONObject(ResData.r.getString(R.string.param_start_location));

            route.distance = new Distance(jsonDistance.getString(ResData.r.getString(R.string.param_text)), jsonDistance.getInt(ResData.r.getString(R.string.param_value)));
            route.duration = new Duration(jsonDuration.getString(ResData.r.getString(R.string.param_text)), jsonDuration.getInt(ResData.r.getString(R.string.param_value)));
            //route.endAddress = jsonLeg.getString("end_address");
            //route.startAddress = jsonLeg.getString("start_adress");
            route.endLocation = new LatLng(jsonStartLocation.getDouble(ResData.r.getString(R.string.param_lat)), jsonStartLocation.getDouble(ResData.r.getString(R.string.param_lng)));
            route.startLocation = new LatLng(jsonEndLocation.getDouble(ResData.r.getString(R.string.param_lat)), jsonStartLocation.getDouble(ResData.r.getString(R.string.param_lng)));
            route.encodedPolyline = overview_polylineJSON.getString(ResData.r.getString(R.string.param_points));

            routes.add(route);
        }
        listener.onDirectionRequestSuccess(routes);
    }
}
