package app.amazing.tramrunner.googledirections;

/**
 * Created by Delia on 20.01.2017.
 */

/**
 * Dauer einer Strecke
 */
public class Duration {
    private String text;
    private int value;

    public Duration(String text, int value) {
        this.text = text;
        this.value = value;
    }

    public String getText() {
        return text;
    }

    public int getValue() {
        return value;
    }
}
