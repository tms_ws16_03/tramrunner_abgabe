package app.amazing.tramrunner.location;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;

import app.amazing.tramrunner.runningProgress.RunningProgressbar;

/**
 * Created by igor on 08.01.17.
 */
public class LocationContainer {
    private static LocationContainer ourInstance = new LocationContainer();
    private static Context appContext = null;
    private static LocationManager locationManager = null;
    private static Location lastKnownLocation = null;

    public static LocationContainer getInstance() {
        return ourInstance;
    }

    public static LocationContainer getInstance(Activity c) {

        if (appContext == null) {
            appContext = c.getApplicationContext();
            locationManager = (LocationManager) appContext.getSystemService(appContext.LOCATION_SERVICE);
        }

        return ourInstance;
    }

    public static LocationManager getLocationManager() {
        return locationManager;
    }

    public static void getLocationOnes(Activity activity) {
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 0.0f, new OneTimeLocationListener(activity));
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public static RunningLocationListener updateLocationUntilFinished(Activity activity, double latitude, double longitude, long departureTime, RunningProgressbar bar) {
        RunningLocationListener listener = new RunningLocationListener(activity, latitude, longitude, departureTime, bar);
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000L, 0.0f, listener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return listener;
    }

    private LocationContainer() {
    }

    public static Location getLastKnownLocation() {
        return lastKnownLocation;
    }

    public static void setLastKnownLocation(Location lastKnownLocation) {
        LocationContainer.lastKnownLocation = lastKnownLocation;
    }
}
