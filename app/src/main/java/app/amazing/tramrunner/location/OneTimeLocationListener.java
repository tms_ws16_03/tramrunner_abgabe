package app.amazing.tramrunner.location;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import app.amazing.tramrunner.MainActivity;
import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.VbbApiHelper;

/**
 * Created by igor on 09.01.17.
 */

public class OneTimeLocationListener implements LocationListener {

    private MainActivity view = null;

    public OneTimeLocationListener(Activity c) {
        view = (MainActivity) c;
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location.getAccuracy() < 30.0f) {
            LocationContainer.setLastKnownLocation(location);

            try {
                LocationContainer.getLocationManager().removeUpdates(this);
            } catch (SecurityException e) {
                e.printStackTrace();
            }

            VbbApiHelper.getInstance().getStationsNearby(String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), view.getAdapterStops());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        //Log.d("[ENABLED]", "PROVIDER");
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Toast.makeText(view, "GPS DISABLED", Toast.LENGTH_LONG).show();

        AlertDialog.Builder builder = new AlertDialog.Builder(view);
        builder.setTitle(ResData.r.getString(R.string.enable_gps));
        builder.setPositiveButton(ResData.r.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                view.finish();
                dialog.dismiss();
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                view.finish();
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        if (!view.isFinishing()) {
            dialog.show();
        }

        //Log.d("[DISABLED]", "PROVIDER");
    }
}
