package app.amazing.tramrunner.runningProgress;

import app.amazing.tramrunner.fragments.RunActiveProgressFragment;

/**
 * Created by Gee on 24.01.2017.
 */

public class RunningProgressbar {

    private int maxValue = 0;
    private int minValue = 0;
    private int runnerValue = 0;
    private int tramValue = 0;

    RunActiveProgressFragment runningActivity;


    public RunningProgressbar(RunActiveProgressFragment runningActivity) {
        minValue = 0;
        maxValue = 100;
        runnerValue = 0;
        tramValue = 0;
        this.runningActivity = runningActivity;
    }


    public void updateTramProgress(int tramValue) {
        if (tramValue <= maxValue && tramValue >= minValue)
            this.tramValue = tramValue;
        if (tramValue < minValue)
            this.tramValue = minValue;
        else if (tramValue > maxValue)
            this.tramValue = maxValue;

        updateProgressbar();
    }

    public void updateTramProgress(float tramValue) {
        updateTramProgress((int) (tramValue * 100));
    }

    public void updateRunnerProgress(int runnerValue) {
        if (runnerValue <= maxValue && runnerValue >= minValue)
            this.runnerValue = runnerValue;
        if (runnerValue < minValue)
            this.runnerValue = minValue;
        else if (runnerValue > maxValue)
            this.runnerValue = maxValue;

        updateProgressbar();
    }

    public void updateRunnerProgress(float runnerValue) {
        updateRunnerProgress((int) (runnerValue * 100));
    }

    public int getTramProgress() {
        return tramValue;
    }

    public int getRunnerProgress() {
        return runnerValue;
    }

    private void updateProgressbar() {
        runningActivity.setRunnerProgress(this.runnerValue);
        runningActivity.setTramProgress(this.tramValue);
    }
}
