package app.amazing.tramrunner.vbb;

import android.widget.ArrayAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;
import app.amazing.tramrunner.vbb.vbbdata.Departure;
import app.amazing.tramrunner.vbb.vbbdata.Station;

/**
 * This Class parses all needed JSON strings.
 */

public class OwnJSONParser {

    /**
     * This method parses the response to a departures ArrayAdapter
     * @param json the response
     * @param departures the destination adapter
     */
    public static void getDepartures(String json, ArrayAdapter<Departure> departures) {
        JSONArray trains = null;
        departures.clear();
        departures.add(new Departure(ResData.r.getString(R.string.vbb_api_itemcode_select_one)));
        try {
            trains = jsonArrayFromString(json);
            Departure departure = null;

            if(trains.length() > 0) {
                for (int i = 0; i < trains.length(); i++) {
                    departure = new Departure();
                    JSONObject temp = trains.getJSONObject(i);
                    departure.setWhen(temp.getString(ResData.r.getString(R.string.vbb_api_param_when)));
                    departure.setDirection(temp.getString(ResData.r.getString(R.string.vbb_api_param_direction)));
                    departure.setLine(temp.getJSONObject(ResData.r.getString(R.string.vbb_api_param_product)).getString(ResData.r.getString(R.string.vbb_api_param_line)));
                    departure.setTrip(temp.getString(ResData.r.getString(R.string.vbb_api_param_trip)));
                    departures.add(departure);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method parses the response to a departures ArrayList
     * @param json the response
     * @param departures the destination list
     */
    public static void getDeparturesToList(String json, ArrayList<Departure> departures) {
        JSONArray trains = null;
        try {
            trains = jsonArrayFromString(json);
            Departure departure = null;

            if(trains.length() > 0) {
                for (int i = 0; i < trains.length(); i++) {
                    departure = new Departure();
                    JSONObject temp = trains.getJSONObject(i);
                    departure.setWhen(temp.getString(ResData.r.getString(R.string.vbb_api_param_when)));
                    departure.setDirection(temp.getString(ResData.r.getString(R.string.vbb_api_param_direction)));
                    departure.setLine(temp.getJSONObject(ResData.r.getString(R.string.vbb_api_param_product)).getString(ResData.r.getString(R.string.vbb_api_param_line)));
                    departure.setTrip(temp.getString(ResData.r.getString(R.string.vbb_api_param_trip)));
                    departures.add(departure);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method parses the response to an adapter
     * @param json the response
     * @param stops the destination adapter
     */
    public static void getStopLocationList(String json, ArrayAdapter<Station> stops) {
        JSONArray stations = null;
        stops.clear();
        stops.add(new Station(ResData.r.getString(R.string.vbb_api_itemcode_select_one)));
        try {
            stations = jsonArrayFromString(json);
            Station stop = null;

            if (stations.length() > 0) {

                for(int i = 0; i< stations.length(); i++) {
                    stop = new Station();
                    JSONObject temp = stations.getJSONObject(i);
                    Iterator<String> keys = temp.keys();
                    String key = null;
                    while(keys.hasNext()) {
                        key = keys.next();
                        // cases have to be constant which is not given by our approach
                        // this strings have to remain hardcoded
                        switch (key) {
                            case "id": stop.setId(temp.getString(key)); break;
                            case "name": stop.setName(temp.getString(key)); break;
                            case "latitude": stop.setLatitude(temp.getString(key)); break;
                            case "longitude": stop.setLongitude(temp.getString(key)); break;
                            case "distance": stop.setDistance(temp.getString(key)); break;
                            default: break;
                        }
                    }
                    stops.add(stop);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method parses a response to an ArrayList
     * @param json the response
     * @return the list of stations
     */
    public static ArrayList<Station> getStopLocationAsList(String json) {
        JSONArray stations = null;
        ArrayList<Station> stops = null;
        try {
            stations = jsonArrayFromString(json);
            Station stop = null;

            if (stations.length() > 0) {

                stops = new ArrayList<Station>();

                for(int i = 0; i< stations.length(); i++) {
                    stop = new Station();
                    JSONObject temp = stations.getJSONObject(i);
                    Iterator<String> keys = temp.keys();
                    String key = null;
                    while(keys.hasNext()) {
                        key = keys.next();
                        // cases have to be constant which is not given by our approach
                        // this strings have to remain hardcoded
                        switch (key) {
                            case "id": stop.setId(temp.getString(key)); break;
                            case "name": stop.setName(temp.getString(key)); break;
                            case "latitude": stop.setLatitude(temp.getString(key)); break;
                            case "longitude": stop.setLongitude(temp.getString(key)); break;
                            case "distance": stop.setDistance(temp.getString(key)); break;
                            default: break;
                        }
                    }
                    stops.add(stop);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return stops;
    }

    private static JSONArray jsonArrayFromString(String json) throws JSONException {
        return new JSONArray(json);
    }
}
