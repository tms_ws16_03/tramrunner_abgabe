package app.amazing.tramrunner.vbb.vbbdata;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;

/**
 * Created by igor on 07.01.17.
 */

public class Departure {

    private String when = "";
    private String direction = "";
    private String line = "";
    private String trip = "";
    private String time = "";

    public Departure() {}

    public Departure(String trip) {
        this.trip = trip;
        if (trip.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_select_one))) {
            this.direction = ResData.r.getString(R.string.vbb_api_itemcode_select_one_value);
        } else if (trip.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_not_found))) {
            this.direction = ResData.r.getString(R.string.vbb_api_itemcode_not_found_value);
        } else if (trip.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_network_error))) {
            this.direction = ResData.r.getString(R.string.vbb_api_itemcode_network_error_value);
        } else {
            this.direction = ResData.r.getString(R.string.vbb_api_itemcode_not_found_value);
        }
    }

    public void copy(Departure departure) {
        when = departure.getWhen();
        direction = departure.getDirection();
        line = departure.getLine();
        trip = departure.getTrip();
        time = departure.getTime();
    }

    @Override
    public String toString() {
        int cond = Integer.valueOf(trip);
        if (cond < 0) {
            return direction;
        } else {
            return String.format(ResData.r.getString(R.string.vbb_api_departure_format), line, time, direction);
        }
    }

    public String getWhen() {
        return when;
    }

    public void setWhen(String when) {
        this.when = when;
        Date time = new Date(Long.valueOf(when) * 1000L);
        DateFormat df = SimpleDateFormat.getTimeInstance(DateFormat.SHORT);
        this.time = df.format(time);
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
