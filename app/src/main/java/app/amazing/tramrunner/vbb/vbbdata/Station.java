package app.amazing.tramrunner.vbb.vbbdata;

import app.amazing.tramrunner.R;
import app.amazing.tramrunner.data.ResData;

/**
 * Created by igor on 31.12.16.
 */

public class Station {
    private String id = "";
    private String name = "";
    private String latitude = "";
    private String longitude = "";
    private String distance = "";

    public Station() {}

    public Station(String id) {
        this.id = id;
        if (id.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_select_one))) {
            this.name = ResData.r.getString(R.string.vbb_api_itemcode_select_one_value);
        } else if (id.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_not_found))) {
            this.name = ResData.r.getString(R.string.vbb_api_itemcode_not_found_value);
        } else if (id.equalsIgnoreCase(ResData.r.getString(R.string.vbb_api_itemcode_network_error))) {
            this.name = ResData.r.getString(R.string.vbb_api_itemcode_network_error_value);
        } else {
            this.name = ResData.r.getString(R.string.vbb_api_itemcode_not_found_value);
        }
    }

    public void copy(Station station) {
        id = station.getId();
        name = station.getName();
        latitude = station.getLatitude();
        longitude = station.getLongitude();
        distance = station.getDistance();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return name;
    }
}
